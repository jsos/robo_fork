import logging
import george
import numpy as np

import os
import time
import json

from robo.priors.default_priors import DefaultPrior
from robo.models.gaussian_process import GaussianProcess
from robo.models.gaussian_process_mcmc import GaussianProcessMCMC
from robo.maximizers.direct import Direct
from robo.maximizers.cmaes import CMAES
from robo.solver.bayesian_optimization import BayesianOptimization
from robo.acquisition_functions.ei import EI
from robo.acquisition_functions.pi import PI
from robo.acquisition_functions.log_ei import LogEI
from robo.acquisition_functions.lcb import LCB
from robo.acquisition_functions.lcb_target import LCBTarget
from robo.acquisition_functions.marginalization import MarginalizationGPMCMC

logger = logging.getLogger(__name__)

def bayesian_step(lower, upper, X, y, num_iterations=30,
						  maximizer="direct", acquisition_func="log_ei", model_type="gp_mcmc",
						  n_init=3, rng=None, output_path=None, do_optimize=True, target=0.0):
	"""
	General interface for Bayesian optimization for global black box
	optimization problems.

	Parameters
	----------
	objective_function: function
		The objective function that is minimized. This function gets a numpy
		array (D,) as input and returns the function value (scalar)
	lower: np.ndarray (D,)
		The lower bound of the search space
	upper: np.ndarray (D,)
		The upper bound of the search space
	num_iterations: int
		The number of iterations (initial design + BO)
	maximizer: {"direct", "cmaes"}
		Defines how the acquisition function is maximized. NOTE: "cmaes" only
		works in D > 1 dimensions
	acquisition_func: {"ei", "log_ei", "lcb", "pi"}
		The acquisition function
	model_type: {"gp", "gp_mcmc", "rf"}
		The model for the objective function.
	n_init: int
		Number of points for the initial design. Make sure that it
		is <= num_iterations.
	output_path: string
		Specifies the path where the intermediate output after each iteration will be saved.
		If None no output will be saved to disk.
	rng: numpy.random.RandomState
		Random number generator

	Returns
	-------
		dict with all results
	"""
	assert upper.shape[0] == lower.shape[0], "Dimension mismatch"
	assert np.all(lower < upper), "Lower bound >= upper bound"
#    assert n_init <= num_iterations, "Number of initial design point has to be <= than the number of iterations"

	if rng is None:
		rng = np.random.RandomState(np.random.randint(0, 10000))

	cov_amp = 2
	n_dims = lower.shape[0]

	initial_ls = np.ones([n_dims])
	exp_kernel = george.kernels.Matern52Kernel(initial_ls,
											   ndim=n_dims)
	kernel = cov_amp * exp_kernel

	prior = DefaultPrior(len(kernel) + 1)

	n_hypers = 3 * len(kernel)
	if n_hypers % 2 == 1:
		n_hypers += 1

	if model_type == "gp":
		model = GaussianProcess(kernel, prior=prior, rng=rng,
								normalize_output=False, normalize_input=True,
								lower=lower, upper=upper)
	elif model_type == "gp_mcmc":
		model = GaussianProcessMCMC(kernel, prior=prior,
									n_hypers=n_hypers,
									chain_length=200,
									burnin_steps=100,
									normalize_input=True,
									normalize_output=False,
									rng=rng, lower=lower, upper=upper,
									noise=2)

	elif model_type == "rf":
		from robo.models.random_forest import RandomForest
		model = RandomForest(types=np.zeros([n_dims]), rng=rng)

	else:
		raise ValueError("'{}' is not a valid model".format(model_type))

	if acquisition_func == "ei":
		a = EI(model)
	elif acquisition_func == "log_ei":
		a = LogEI(model)
	elif acquisition_func == "pi":
		a = PI(model)
	elif acquisition_func == "lcb":
		if target is not None:
			a = LCBTarget(model, target=target, par=1.0) #todo remove
		else:
			a = LCB(model, par=1.0)
	else:
		raise ValueError("'{}' is not a valid acquisition function"
						 .format(acquisition_func))

	if acquisition_func != "lcb" and target is not None:
		raise ValueError("Cannot use target optimization with non-LCB acquistion function")

	if model_type == "gp_mcmc":
		acquisition_func = MarginalizationGPMCMC(a)
	else:
		acquisition_func = a

	if maximizer == "cmaes":
		max_func = CMAES(acquisition_func, lower, upper, verbose=False,
						 rng=rng)
	elif maximizer == "direct":
		max_func = Direct(acquisition_func, lower, upper, verbose=False)
	else:
		raise ValueError("'{}' is not a valid function to maximize the "
						 "acquisition function".format(maximizer))

	#bo = BayesianOptimization(objective_function, lower, upper,
	#                          acquisition_func, model, max_func,
	#                          initial_points=n_init, rng=rng,
	#                          output_path=output_path)

	#x_best, f_min = bo.run(num_iterations)

########################

	time_start = time.time()

	if X is None and y is None:
		raise ValueError("Need non-empty values for X and y");
	logger.info("Starting optimization step")

	start_time = time.time()

	# Choose next point to evaluate
	new_x, m, v = choose_next(model, acquisition_func, max_func, lower, upper, X, y, do_optimize)

	logger.info("Optimization overhead was %f seconds", time.time() - start_time)
	logger.info("Next candidate %s", str(new_x))

	# # Evaluate
	# start_time = time.time()
	# new_y = self.objective_func(new_x)
	# self.time_func_evals.append(time.time() - start_time)

	# logger.info("Configuration achieved a performance of %f ", new_y)
	# logger.info("Evaluation of this configuration took %f seconds", self.time_func_evals[-1])

	# # Extend the data
	# self.X = np.append(self.X, new_x[None, :], axis=0)
	# self.y = np.append(self.y, new_y)

	# Estimate incumbent
	if target is not None:
		best_idx = np.argmin(np.abs(y-target))
	else:
		best_idx = np.argmin(y)
	incumbent = X[best_idx]
	incumbent_value = y[best_idx]

	# self.incumbents.append(incumbent.tolist())
	# self.incumbents_values.append(incumbent_value)
	logger.info("Current incumbent %s with estimated performance %f",
				str(incumbent), incumbent_value)

	# runtime.append(time.time() - start_time)

	if output_path is not None:
		save_output(it)

	# logger.info("Return %s as incumbent with error %f ",
	#             self.incumbents[-1], self.incumbents_values[-1])

	return new_x, incumbent, incumbent_value, m, v






########################


	# results = dict()
	# results["x_opt"] = x_best
	# results["f_opt"] = f_min
	# results["x_opt"] = x_best
	# # results["incumbents"] = [inc for inc in bo.incumbents]
	# # results["incumbent_values"] = [val for val in bo.incumbents_values]
	# # results["runtime"] = bo.runtime
	# # results["overhead"] = bo.time_overhead
	# # results["X"] = bo.X
	# # results["y"] = bo.y
	# return results

def choose_next(model, acquisition_func, maximize_func, lower, upper, X=None, y=None, do_optimize=True):
	"""
	Suggests a new point to evaluate.

	Parameters
	----------
	X: np.ndarray(N,D)
		Initial points that are already evaluated
	y: np.ndarray(N,1)
		Function values of the already evaluated points
	do_optimize: bool
		If true the hyperparameters of the model are
		optimized before the acquisition function is
		maximized.
	Returns
	-------
	np.ndarray(1,D)
		Suggested point
	"""
	if X is None and y is None:
		raise ValueError('Need non-empty values for X and y');

	elif X.shape[0] == 1:
		raise ValueError('Need at least 2 initial data points for training');

	else:
		try:
			logger.info("Train model...")
			t = time.time()
			model.train(X, y, do_optimize=do_optimize)
			logger.info("Time to train the model: %f", (time.time() - t))
		except:
			logger.error("Model could not be trained!")
			raise

		try:
			space = np.linspace(lower[0],upper[0],(upper[0] - lower[0])/0.05 + 1)
			space = np.expand_dims(space,1)
			m,v = model.predict(space)
		except:
			logger.error("Model could not be predicted!")
			raise

		acquisition_func.update(model)

		logger.info("Maximize acquisition function...")
		t = time.time()
		x = maximize_func.maximize()

		acquisition = acquisition_func.compute(space)


		logger.info("Time to maximize the acquisition function: %f", (time.time() - t))

	return x, m, v

import argparse
parser = argparse.ArgumentParser()
parser.add_argument('-X', help='<Required> json array with parameter history', required=True, type=str)
parser.add_argument('-y', help='<Required> json array with y value history', required=True, type=str)
parser.add_argument('-lower', help='<Required> json array with lower bounds', required=True, type=str)
parser.add_argument('-upper', help='<Required> json array with upper', required=True, type=str)
parser.add_argument('-target', help='<Required> json array with target value', required=False, type=str)
parser.add_argument('-seed', help='Use this seed in python', required=False, type=int)
parser.add_argument('-acq_f', help='Acquisition function: lcb|pi|ei|log_ei', required=False, type=str, default='log_ei')
parser.add_argument('-full_output', help='output mean+var?', action='store_true')

args = parser.parse_args()
rng_state = np.random.RandomState(args.seed)
np.random.seed(args.seed)

#np.random.RandomState = "error";
#np.random = "asdf"
#print(rng_state.rand(1))
X = np.array(json.loads(args.X))
y = np.array(json.loads(args.y))
lower = np.array(json.loads(args.lower))
upper = np.array(json.loads(args.upper))
if args.target is not None:
	target = np.float(json.loads(args.target))
else:
	target = None
model_type = "gp_mcmc"#gp_mcmc | gp
x, inc, inc_f, m, v = bayesian_step(lower, upper, X, y, rng=rng_state, acquisition_func=args.acq_f, target=target, model_type=model_type)
results = {}
results['new_x'] = x.tolist()
results['incumbent_x'] = inc.tolist()
results['incumbent_f'] = inc_f.tolist()
if(args.full_output):
	results['mean'] = m.tolist()
	results['variance'] = v.tolist()

print(json.dumps(results))