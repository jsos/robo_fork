from flask import Flask, jsonify, request, abort, json
from urllib.parse import unquote as unq

import numpy as np
import socket
import random
import time
import os
import argparse
import signal
import sys
from bayesian_step import bayesian_step

import json
import logging

from robo.priors.base_prior import TophatPrior, LognormalPrior, NormalPrior, HorseshoePrior

# MAX_RETRIES = 20
app = Flask(__name__)

logging.basicConfig(level=logging.DEBUG)
# logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

def signal_handler(signal, frame):
        logger.info("Received SIGINT. RoBO REST server is shutting down.")
        sys.exit(0)

def parse_prior_list(prior_list):
	PRIOR_HYPERPARAMETERS = ['lengthscale', 'covariance', 'noise']
	lengthscale_prior = covariance_prior = noise_prior = None
	for i,item in enumerate(prior_list):
		logger.debug(item)
		if item == 'lengthscale':
			lengthscale_prior = parse_prior(prior_list,i)
		elif item == 'covariance':
			covariance_prior = parse_prior(prior_list,i)
		elif item == 'noise':
			noise_prior = parse_prior(prior_list,i)

	return lengthscale_prior, covariance_prior, noise_prior

def parse_prior(prior_list,i):
	PRIOR_TYPES = ['tophat', 'lognormal', 'normal', 'horseshoe']
	prior = None
	if prior_list[i+1] == 'tophat':
		prior = TophatPrior(prior_list[i+2],prior_list[i+3])
	elif prior_list[i+1] == 'lognormal':
		prior = LognormalPrior(prior_list[i+2],prior_list[i+3])
	elif prior_list[i+1] == 'normal':
		prior = NormalPrior(prior_list[i+2],prior_list[i+3])
	elif prior_list[i+1] == 'horseshoe':
		prior = HorseshoePrior(prior_list[i+2])
	else:
		raise ValueError('Could not parse the type of the prior.')
	return prior

@app.route('/robo/api/step-url', methods=['GET'])
def make_step_url():
	try:
		validate_request_url(request)
	except ValueError as e:
		return jsonify({'status': 'error', 'content': str(e)}), 400

	q = request.args
	try:
		X = np.array(json.loads(unq(q.get('X'))))
		y = np.array(json.loads(unq(q.get('y'))))
		lower = np.array(json.loads(unq(q.get('lower'))))
		upper = np.array(json.loads(unq(q.get('upper'))))
	except ValueError as e:
		return jsonify({'status': 'error', 'content': 'Could not parse required json data from URI.'}), 400
	try:
		if q.get('target'):
			target = np.array(json.loads(unq(q.get('target'))))
		else:
			target = None
		if q.get('seed'):
			seed = int(unq(q.get('seed')))
		else:
			seed = None
		if q.get('kappa'):
			kappa = float(unq(q.get('kappa')))
		else:
			kappa = 1.0
		if q.get('mcmc_pos'):
			mcmc_pos = json.loads(q.get('mcmc_pos'))
			if mcmc_pos is not None:
				mcmc_pos = np.array(mcmc_pos)
		else:
			mcmc_pos = None
		if q.get('do_optimize'):
			do_optimize = json.loads(q.get('do_optimize'))
			if do_optimize is not None:
				do_optimize = bool(int(do_optimize)) # accept both True and 1 as truthy values
		else:
			do_optimize = True
		if q.get('prob_acq'):
			prob_acq = json.loads(q.get('prob_acq'))
			if prob_acq is not None:
				prob_acq = bool(int(prob_acq)) # accept both True and 1 as truthy values
		else:
			prob_acq = False
		if q.get('prob_bonus'):
			prob_bonus = json.loads(q.get('prob_bonus'))
		else:
			prob_bonus = 1.0
		if q.get('top_bottom_ratio'):
			top_bottom_ratio = json.loads(q.get('top_bottom_ratio'))
		else:
			top_bottom_ratio = 9.0
		if q.get('top_bottom_threshold'):
			top_bottom_threshold = json.loads(q.get('top_bottom_threshold'))
		else:
			top_bottom_threshold = 0.9
		if q.get('prior_list'):
			lengthscale_prior, covariance_prior, noise_prior = parse_prior_list(json.loads(unq(q.get('prior_list'))))
			logger.info('Using custom priors: %r', (lengthscale_prior, covariance_prior, noise_prior))
		else:
			lengthscale_prior = covariance_prior = noise_prior = None
			logger.warning('Using default RoBO priors.')

		rng_state = np.random.RandomState(seed)
		np.random.seed(seed)
		model_type = "gp_mcmc"
		#full_output = bool(int(q.get('full_output')))
	except ValueError as e:
		raise e
		logger.error(str(e))
		return jsonify({'status': 'error', 'content': str(e)}), 400

	x, inc, inc_f, m, v, acquisition, new_mcmc_pos, chain_std = bayesian_step(lower, upper, X, y, 
				      rng=rng_state, acquisition_func='lcb', do_optimize=do_optimize, target=target, 
					  model_type=model_type, kappa=kappa, mcmc_pos=mcmc_pos, prob_acq=prob_acq,
					  top_bottom_threshold=top_bottom_threshold, top_bottom_ratio=top_bottom_ratio,
					  prob_bonus=prob_bonus, lengthscale_prior=lengthscale_prior, covariance_prior=covariance_prior,
					  noise_prior=noise_prior)
	results = {}

	results['new_x'] = x.tolist()
	results['incumbent_x'] = inc.tolist()
	results['incumbent_f'] = inc_f.tolist()
	#if 'full_output' in json_data and json_data['full_output'] == True:
	results['mean'] = m.tolist()
	results['variance'] = v.tolist()
	results['acquisition'] = acquisition.tolist()
	if new_mcmc_pos is not None:
		results['mcmc_pos'] = new_mcmc_pos.tolist()
	else:
		results['mcmc_pos'] = None
	results['chain_std'] = chain_std.tolist()

	return(jsonify({'status': 'success', 'content': results}))

@app.route('/robo/api/step-dry', methods=['GET'])
def make_step_dry():
	print(request.get_json())
	try:
		validate_request_json(request)
	except ValueError as e:
		return jsonify({'status': 'error', 'content': str(e)}), 400
	return(jsonify({'status': 'success', 'content': 'Success in parameter validation during dry run.'}))


@app.route('/robo/api/connection-test', methods=['GET'])
def connection_working():
	return jsonify({'status': 'Connection possible.'})

def validate_request_url(request):
	q = request.args
	if not q.get('X'):
		raise ValueError('Need X values.')
	if not q.get('y'):
		raise ValueError('Need y values.')
	if not q.get('lower') or not q.get('upper'):
		raise ValueError('Need both lower and upper bounds.')
	if not q.get('kappa'):
		raise ValueError('Supply kappa parameter for acquisition function.')


if __name__ == '__main__':
	signal.signal(signal.SIGINT, signal_handler)
	parser = argparse.ArgumentParser(description='Start the RoBO stateless REST-Server.')
	parser.add_argument('port', metavar='port', type=int, nargs='?',
	                    help='The port number for the REST server.')
	args = parser.parse_args()
	if args.port is not None:
		app.run(debug=False, port=int(args.port), host='0.0.0.0')
	else:
		app.run(debug=False, host='0.0.0.0')
		
