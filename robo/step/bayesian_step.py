import logging
import george
import numpy as np

import os
import time

from robo.priors.default_priors import DefaultPrior
from robo.priors.bci_prior import BCIPrior
from robo.models.gaussian_process import GaussianProcess
from robo.models.gaussian_process_mcmc import GaussianProcessMCMC
from robo.maximizers.direct import Direct
from robo.maximizers.cmaes import CMAES
from robo.solver.bayesian_optimization import BayesianOptimization
from robo.acquisition_functions.ei import EI
from robo.acquisition_functions.pi import PI
from robo.acquisition_functions.log_ei import LogEI
from robo.acquisition_functions.lcb import LCB
from robo.acquisition_functions.lcb_target import LCBTarget
from robo.acquisition_functions.marginalization import MarginalizationGPMCMC

import DIRECT

logger = logging.getLogger(__name__)

def bayesian_step(lower, upper, X, y,
				  maximizer="direct", acquisition_func="log_ei", model_type="gp_mcmc",
				  n_init=3, rng=None, output_path=None, do_optimize=True, target=0.0, kappa=1.0, mcmc_pos=None,
				  prob_acq=False, top_bottom_threshold=None, top_bottom_ratio=None, prob_bonus=None,
				  lengthscale_prior=None, covariance_prior=None, noise_prior=None, kernel_string=None ):
	"""
	General interface for Bayesian optimization for global black box
	optimization problems. This function performs optimization on a
	step-by-step basis. Note that you must provide the last state of
	the MCMC model to have a valid optimization procedure except for
	the first iteration. As a result, this interface can be regarded
	as stateless for optimization purposes.

	Parameters
	----------
	objective_function: function
		The objective function that is minimized. This function gets a numpy
		array (D,) as input and returns the function value (scalar)
	lower: np.ndarray (D,)
		The lower bound of the search space
	upper: np.ndarray (D,)
		The upper bound of the search space
	maximizer: {"direct", "cmaes"}
		Defines how the acquisition function is maximized. NOTE: "cmaes" only
		works in D > 1 dimensions
	acquisition_func: {"ei", "log_ei", "lcb", "pi"}
		The acquisition function
	model_type: {"gp", "gp_mcmc", "rf"}
		The model for the objective function.
	n_init: int
		Number of points for the initial design. Make sure that it
		is <= num_iterations.
	output_path: string
		Specifies the path where the intermediate output after each iteration will be saved.
		If None no output will be saved to disk.
	rng: numpy.random.RandomState
		Random number generator
	do_optimize: bool
		If true, the hyper parameters of the acquisition function are
		optimized prior to maximization
	mcmc_pos: np.ndarray
		Contains the state of the Markov Chain Monte Carlo system. This state
		is returned after each call and has to be stored on the client side.
	prob_acq: bool
		If true, the acquisition funciton is sampled probabilistically. Otherwise, the
		deterministic optimum is chosen.



	Returns
	-------
		dict with all results
	"""
	assert upper.shape[0] == lower.shape[0], "Dimension mismatch"
	assert np.all(lower < upper), "Lower bound >= upper bound"

	if rng is None:
		rng = np.random.RandomState(np.random.randint(0, 10000))

	cov_amp = 2
	n_dims = lower.shape[0]

	initial_ls = np.ones([n_dims])
	# Default matern52
	raw_kernel = george.kernels.Matern52Kernel(initial_ls,
											   ndim=n_dims)
	if not kernel_string is None:
		if kernel_string == 'matern52':
			raw_kernel = george.kernels.Matern52Kernel(initial_ls,
											   ndim=n_dims)
			logger.info('Using specified Matern52 Kernel.')
		elif kernel_string == 'matern32':
			raw_kernel = george.kernels.Matern32Kernel(initial_ls,
											   ndim=n_dims)
			logger.info('Using specified Matern32 Kernel.')
		elif kernel_string == 'exp_squared':
			raw_kernel = george.kernels.ExpSquaredKernel(initial_ls,
											   ndim=n_dims)
			logger.info('Using specified Exponential Squared Kernel.')
		elif kernel_string == 'exp':
			raw_kernel = george.kernels.ExpKernel(initial_ls,
											   ndim=n_dims)
			logger.info('Using specified Exponential Kernel.')
		else:
			raise ValueError("'{}' is not a valid kernel string".format(kernel_string))


	kernel = cov_amp * raw_kernel

	prior = BCIPrior(len(kernel) + 1,
			lengthscale_prior=lengthscale_prior,
			covariance_prior=covariance_prior,
			noise_prior=noise_prior)

	n_hypers = 3 * len(kernel)
	if n_hypers % 2 == 1:
		n_hypers += 1

	# Double number of internal gaussian process representations
	n_hypers *= 2

	if model_type == "gp":
		model = GaussianProcess(kernel, prior=prior, rng=rng,
								normalize_output=False, normalize_input=True,
								lower=lower, upper=upper)
	elif model_type == "gp_mcmc":
		model = GaussianProcessMCMC(kernel, prior=prior,
									n_hypers=n_hypers,
									chain_length=200,
									burnin_steps=100,
									normalize_input=True,
									normalize_output=False,
									rng=rng, lower=lower-100, upper=upper+100)

	elif model_type == "rf":
		from robo.models.random_forest import RandomForest
		model = RandomForest(types=np.zeros([n_dims]), rng=rng)

	else:
		raise ValueError("'{}' is not a valid model".format(model_type))

	if acquisition_func == "ei":
		a = EI(model)
	elif acquisition_func == "log_ei":
		a = LogEI(model)
	elif acquisition_func == "pi":
		a = PI(model)
	elif acquisition_func == "lcb":
		if target is not None:
			a = LCBTarget(model, target=target, par=kappa)
		else:
			a = LCB(model, par=kappa)
	else:
		raise ValueError("'{}' is not a valid acquisition function"
						 .format(acquisition_func))

	if acquisition_func != "lcb" and target is not None:
		raise ValueError("Cannot use target optimization with non-LCB acquistion function")

	if model_type == "gp_mcmc":
		acquisition_func = MarginalizationGPMCMC(a)
	else:
		acquisition_func = a

	if maximizer == "cmaes":
		max_func = CMAES(acquisition_func, lower, upper, verbose=False,
						 rng=rng)
	elif maximizer == "direct":
		max_func = Direct(acquisition_func, lower, upper, verbose=False)
	else:
		raise ValueError("'{}' is not a valid function to maximize the "
						 "acquisition function".format(maximizer))

	if X is None and y is None:
		raise ValueError("Need non-empty values for X and y");
	logger.info("Starting optimization step")

	start_time = time.time()

	# Choose next point to evaluate
	new_x, m, v, acquisition, new_mcmc_pos, chain_std = choose_next(model,acquisition_func, max_func, lower, upper, X, y,
												 do_optimize, mcmc_pos, prob_acq, top_bottom_threshold, top_bottom_ratio, prob_bonus)

	logger.info("Optimization overhead was %f seconds", time.time() - start_time)
	logger.info("Next candidate %s", str(new_x))

	# Estimate incumbent
	if target is not None:
		best_idx = np.argmin(np.abs(y-target))
	else:
		best_idx = np.argmin(y)
	incumbent = X[best_idx]
	incumbent_value = y[best_idx]

	logger.info("Current incumbent %s with estimated performance %f",
				str(incumbent), incumbent_value)

	if output_path is not None:
		save_output(it)

	return new_x, incumbent, incumbent_value, m, v, acquisition, new_mcmc_pos, chain_std

def choose_next(model, acquisition_func, maximize_func, lower, upper, X=None, y=None, do_optimize=True, mcmc_pos=None,
				prob_acq=False, top_bottom_threshold=None, top_bottom_ratio=None, prob_bonus=None):
	"""
	Suggests a new point to evaluate.

	Parameters
	----------
	X: np.ndarray(N,D)
		Initial points that are already evaluated
	y: np.ndarray(N,1)
		Function values of the already evaluated points
	do_optimize: bool
		If true the hyperparameters of the model are
		optimized before the acquisition function is
		maximized.
	Returns
	-------
	np.ndarray(1,D)
		Suggested point
	"""
	if X is None and y is None:
		raise ValueError('Need non-empty values for X and y');

	elif X.shape[0] == 1:
		raise ValueError('Need at least 2 initial data points for training');

	else:
		try:
			if isinstance(model, GaussianProcessMCMC) and mcmc_pos is not None:
				model.burned = True
				model.p0 = mcmc_pos
				logger.info('Using supplied MCMC state.')
			else:
				logger.info('No MCMC state available, initializing and burning in new state.')
			logger.info("Train model...")
			t = time.time()
			model.train(X, y, do_optimize=do_optimize)
			logger.info("Time to train the model: %f", (time.time() - t))
			if isinstance(model, GaussianProcessMCMC):
				new_mcmc_pos=model.p0
			else:
				new_mcmc_pos=None
			#Investigate why no np.exp necessary
			chain_std = np.std(np.exp(model.chain),1)
			logger.debug('MCMC State in log scale (i.e. sigvar, lengthscale, noisevar):\n %r', mcmc_pos)
			logger.debug('Standard deviation (normal scale, no log) within the chain:\n %r', chain_std)
			# logger.debug(model.chain.shape)
		except:
			logger.error("Model could not be trained!")
			raise

		try:
			space = np.linspace(lower[0],upper[0],max(upper[0]-lower[0]+1,100))
			space = np.expand_dims(space,1)
			m,v = model.predict(space)
		except:
			logger.error("Model could not be predicted!")
			raise

		acquisition_func.update(model)
		acquisition = acquisition_func.compute(space)

		logger.info("Maximize acquisition function...")
		t = time.time()
		probabilistic_acquisition = prob_acq
		# Probabilistic Acquisition
		if probabilistic_acquisition:
			logger.info('Using the probabilistic transformation of the acquisition function.')
			N = int(np.floor(acquisition.shape[0] * top_bottom_threshold))
			acquisition -= min(acquisition)
			acquisition /= max(acquisition)

			acq_sort = np.sort(acquisition)

			# With print
			# def area_function(b, user_data):
			# 	return( (top_bottom_ratio*np.sum(acq_sort[:N]**b) - np.sum(acq_sort[N:]**b))**2 ), 0

			# b, _, _ = DIRECT.solve(area_function,0,1000,algmethod=1)

			# Without print
			def area_function(b):
				return( -(top_bottom_ratio*np.sum(acq_sort[:N]**b) - np.sum(acq_sort[N:]**b))**2 )
			max_func = Direct(area_function, 0, 10000, verbose=False)
			b = max_func.maximize()

			logger.debug('Found transformation exponent b: %f', b)
			acquisition = acquisition**b

			# Add constant penalty: more likelihood for short (left) parameters
			logger.debug(prob_bonus)
			if prob_bonus is not None:
				acquisition *= np.linspace(prob_bonus,1,max(upper[0]-lower[0]+1,100))

			# Renormalize
			acquisition -= min(acquisition)
			acquisition /= max(acquisition)

			# Transform to probabilities and use to draw random parameter
			acquisition = acquisition/np.sum(acquisition)
			x = np.random.choice(np.squeeze(space), p=np.squeeze(acquisition))
		# Maximum acquisition, do not use with parameter_penalty
		else:
			x = maximize_func.maximize() # The real acquisition maximization

		logger.info("Time to maximize the acquisition function: %f", (time.time() - t))
		# logger.debug("Acquisition values: \n %r",acquisition)

	return x, m, v, acquisition, new_mcmc_pos, chain_std