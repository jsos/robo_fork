# A RoBO fork for online optimization in EEG paradigms

This fork adds several changes to make RoBO applicable to the high noise domain of EEG data and the problem of function evaluation. Notably, a step wise optimization interface is provided that does not require an evaluatable python function. Furthermore, you can choose the optimization iteration, when the hyper parameter optimization should start manually. For consideration of evaluation costs, the acquisition functions are provided with the probabilistic acquisition modification.

In order to run this fork you need to create a python virtual environment in a folder at the root of this repository called 'robopy'. Install flask in this 
environment. Then install the requirements for RoBO and finally install RoBO.

## Troubleshooting:

- Problem: Numpy.multiarray not working / numpy was compiled for different API version
- Solution: pip uninstall direct, build your own copy of the [DIRECT](https://pypi.python.org/pypi/DIRECT) library and install it

Original readme below.

RoBO - a Robust Bayesian Optimization framework.
================================================

Documentation
-------------
You can find the documentation for RoBO here http://automl.github.io/RoBO/


Citing RoBO
-----------

To cite RoBO please reference our BayesOpt paper:
```
@INPROCEEDINGS{klein-bayesopt17,
author    = {A. Klein and S. Falkner and N. Mansur and F. Hutter},
title     = {RoBO: A Flexible and Robust Bayesian Optimization Framework in Python},
booktitle = {NIPS 2017 Bayesian Optimization Workshop},
year      = {2017},
month     = dec,
}
```
